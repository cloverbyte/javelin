# Javelin

Demo Application using Spring Boot.

## Installation


## Usage

```python
gradle bootRun
```

## OpenAPI
View the API with this link.
http://localhost:8080/v3/api-docs/

http://localhost:8080/swagger-ui/index.html?url=/v3/api-docs#/

## H2 Console

http://localhost:8080/h2-console

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)