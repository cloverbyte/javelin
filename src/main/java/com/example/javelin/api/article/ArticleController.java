package com.example.javelin.api.article;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/articles")
@Tag(name = "Article", description = "The Article API")
public class ArticleController {
  private final ArticleService articleService;

  @Autowired
  public ArticleController(ArticleService articleService) {
    this.articleService = articleService;
  }

  @Operation(summary = "Create a article", description = "Returns the created article")
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public Article createArticle(@RequestBody Article article) {
    return articleService.createArticle(article);
  }

  @Operation(summary = "Find all articles", description = "Returns all articles")
  @GetMapping
  public List<Article> getAllArticles() {
    return articleService.getAllArticles();
  }

  @Operation(summary = "Find article by ID", description = "Returns a single article.")
  @GetMapping("/{id}")
  public Article getArticle(@PathVariable Long id) {
    return articleService.getArticle(id);
  }

  @Operation(summary = "Update an existing article", description = "")
  @PutMapping("/{id}")
  public Article getArticle(@PathVariable Long id, @RequestBody Article article) {
    return articleService.updateArticle(id, article);
  }

  @Operation(summary = "Deletes a article", description = "")
  @DeleteMapping("/{id}")
  public Article deleteArticle(@PathVariable Long id) {
    return articleService.deleteArticle(id);
  }
}
