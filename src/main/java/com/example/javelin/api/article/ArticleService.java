package com.example.javelin.api.article;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ArticleService {
  private final ArticleRepository articleRepository;

  @Autowired
  public ArticleService(ArticleRepository articleRepository) {
    this.articleRepository = articleRepository;
  }

  public Article createArticle(Article article) {
    return articleRepository.save(article);
  }

  public List<Article> getAllArticles() {
    List<Article> articles = new ArrayList<>();
    articleRepository.findAll().forEach(articles::add);
    return articles;
  }

  public Article getArticle(Long id) {
    return articleRepository.findById(id).orElse(null);
  }

  public Article updateArticle(Long id, Article article) {
    Article articleFromDb = articleRepository.findById(id).orElse(null);
    article.setId(id);
    return articleRepository.save(article);
  }

  public Article deleteArticle(Long id) {
    Article articleFromDb = articleRepository.findById(id).orElse(null);
    articleRepository.deleteById(id);
    return articleFromDb;
  }
}
