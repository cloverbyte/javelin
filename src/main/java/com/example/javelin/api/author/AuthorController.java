package com.example.javelin.api.author;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/authors")
@Tag(name = "Author", description = "The Author API")
public class AuthorController {
  private final AuthorService authorService;

  @Autowired
  public AuthorController(AuthorService authorService) {
    this.authorService = authorService;
  }

  @Operation(summary = "Create a author", description = "Returns the created author")
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public Author createAuthor(@RequestBody Author author) {
    return authorService.createAuthor(author);
  }

  @Operation(summary = "Find all authors", description = "Returns all authors")
  @GetMapping
  public List<Author> getAllAuthors() {
    return authorService.getAllAuthors();
  }

  @Operation(summary = "Find author by ID", description = "Returns a single author.")
  @GetMapping("/{id}")
  public Author getAuthor(@PathVariable Long id) {
    return authorService.getAuthor(id);
  }

  @Operation(summary = "Update an existing author", description = "")
  @PutMapping("/{id}")
  public Author getAuthor(@PathVariable Long id, @RequestBody Author author) {
    return authorService.updateAuthor(id, author);
  }

  @Operation(summary = "Deletes a author", description = "")
  @DeleteMapping("/{id}")
  public Author deleteAuthor(@PathVariable Long id) {
    return authorService.deleteAuthor(id);
  }
}
