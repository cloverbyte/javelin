package com.example.javelin.api.author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthorService {
  private final AuthorRepository authorRepository;

  @Autowired
  public AuthorService(AuthorRepository authorRepository) {
    this.authorRepository = authorRepository;
  }

  public Author createAuthor(Author author) {
    return authorRepository.save(author);
  }

  public List<Author> getAllAuthors() {
    List<Author> authors = new ArrayList<>();
    authorRepository.findAll().forEach(authors::add);
    return authors;
  }

  public Author getAuthor(Long id) {
    return authorRepository.findById(id).orElse(null);
  }

  public Author updateAuthor(Long id, Author author) {
    Author authorFromDb = authorRepository.findById(id).orElse(null);
    author.setId(id);
    return authorRepository.save(author);
  }

  public Author deleteAuthor(Long id) {
    Author authorFromDb = authorRepository.findById(id).orElse(null);
    authorRepository.deleteById(id);
    return authorFromDb;
  }
}
