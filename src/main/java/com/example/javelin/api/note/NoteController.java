package com.example.javelin.api.note;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/notes")
@Tag(name = "Note", description = "The Note API")
public class NoteController {
  private final NoteService noteService;

  @Autowired
  public NoteController(NoteService noteService) {
    this.noteService = noteService;
  }

  @Operation(summary = "Create a note", description = "Returns the created note")
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public Note createNote(@RequestBody Note note) {
    return noteService.createNote(note);
  }

  @Operation(summary = "Find all notes", description = "Returns all notes")
  @GetMapping
  public List<Note> getAllNotes() {
    return noteService.getAllNotes();
  }

  @Operation(summary = "Find note by ID", description = "Returns a single note.")
  @GetMapping("/{id}")
  public Note getNote(@PathVariable Long id) {
    return noteService.getNote(id);
  }

  @Operation(summary = "Update an existing note", description = "")
  @PutMapping("/{id}")
  public Note getNote(@PathVariable Long id, @RequestBody Note note) {
    return noteService.updateNote(id, note);
  }

  @Operation(summary = "Deletes a note", description = "")
  @DeleteMapping("/{id}")
  public Note deleteNote(@PathVariable Long id) {
    return noteService.deleteNote(id);
  }
}
