package com.example.javelin.api.note;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NoteService {
  private final NoteRepository noteRepository;

  @Autowired
  public NoteService(NoteRepository noteRepository) {
    this.noteRepository = noteRepository;
  }

  public Note createNote(Note note) {
    return noteRepository.save(note);
  }

  public List<Note> getAllNotes() {
    List<Note> notes = new ArrayList<>();
    noteRepository.findAll().forEach(notes::add);
    return notes;
  }

  public Note getNote(Long id) {
    return noteRepository.findById(id).orElse(null);
  }

  public Note updateNote(Long id, Note note) {
    Note noteFromDb = noteRepository.findById(id).orElse(null);
    note.setId(id);
    return noteRepository.save(note);
  }

  public Note deleteNote(Long id) {
    Note noteFromDb = noteRepository.findById(id).orElse(null);
    noteRepository.deleteById(id);
    return noteFromDb;
  }
}
