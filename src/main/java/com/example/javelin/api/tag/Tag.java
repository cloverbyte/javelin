package com.example.javelin.api.tag;

import com.example.javelin.api.ticket.Ticket;
import com.example.javelin.common.Auditable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Tag extends Auditable<String> {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String name;

//  @ManyToOne
//  @JoinColumn(name = "ticket_id", nullable = false)
//  private Ticket ticket;


}
