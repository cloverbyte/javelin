package com.example.javelin.api.tag;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tags")
@io.swagger.v3.oas.annotations.tags.Tag(name = "Tag", description = "The Tag API")
public class TagController {
  private final TagService tagService;

  @Autowired
  public TagController(TagService tagService) {
    this.tagService = tagService;
  }

  @Operation(summary = "Create a tag", description = "Returns the created tag")
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public Tag createTag(@RequestBody Tag tag) {
    return tagService.createTag(tag);
  }

  @Operation(summary = "Find all tags", description = "Returns all tags")
  @GetMapping
  public List<Tag> getAllTags() {
    return tagService.getAllTags();
  }

  @Operation(summary = "Find tag by ID", description = "Returns a single tag.")
  @GetMapping("/{id}")
  public Tag getTag(@PathVariable Long id) {
    return tagService.getTag(id);
  }

  @Operation(summary = "Update an existing tag", description = "")
  @PutMapping("/{id}")
  public Tag getTag(@PathVariable Long id, @RequestBody Tag tag) {
    return tagService.updateTag(id, tag);
  }

  @Operation(summary = "Deletes a tag", description = "")
  @DeleteMapping("/{id}")
  public Tag deleteTag(@PathVariable Long id) {
    return tagService.deleteTag(id);
  }
}
