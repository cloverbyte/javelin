package com.example.javelin.api.tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TagService {
  private final TagRepository tagRepository;

  @Autowired
  public TagService(TagRepository tagRepository) {
    this.tagRepository = tagRepository;
  }

  public Tag createTag(Tag tag) {
    return tagRepository.save(tag);
  }

  public List<Tag> getAllTags() {
    List<Tag> tags = new ArrayList<>();
    tagRepository.findAll().forEach(tags::add);
    return tags;
  }

  public Tag getTag(Long id) {
    return tagRepository.findById(id).orElse(null);
  }

  public Tag updateTag(Long id, Tag tag) {
    Tag tagFromDb = tagRepository.findById(id).orElse(null);
    tag.setId(id);
    return tagRepository.save(tag);
  }

  public Tag deleteTag(Long id) {
    Tag tagFromDb = tagRepository.findById(id).orElse(null);
    tagRepository.deleteById(id);
    return tagFromDb;
  }
}
