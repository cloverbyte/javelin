package com.example.javelin.api.task;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@Tag(name = "Task", description = "The Task API")
public class TaskController {
  private final TaskService taskService;

  @Autowired
  public TaskController(TaskService taskService) {
    this.taskService = taskService;
  }

  @Operation(summary = "Create a task", description = "Returns the created task")
  @PostMapping(value = "/tickets/{ticketId}/tasks", consumes = MediaType.APPLICATION_JSON_VALUE)
  public Task createTask(@PathVariable String ticketId, @RequestBody Task task) {
    return taskService.createTask(ticketId, task);
  }

  @Operation(summary = "Find all tasks", description = "Returns all tasks")
  @GetMapping("/tickets/{ticketId}/tasks")
  public Page<Task> getAllTasksInTicket(@PathVariable String ticketId, Pageable pageable) {
    return taskService.getAllTasksInTicket(ticketId, pageable);
  }

//  @Operation(summary = "Find task by ID", description = "Returns a single task.")
//  @GetMapping("/{taskId}")
//  public Task getTask(@PathVariable Long taskId) {
//    return taskService.getTask(taskId);
//  }

  @Operation(summary = "Update an existing task", description = "")
  @PutMapping("/tickets/{ticketId}/tasks/{taskId}")
  public Task getTask(@PathVariable String ticketId, @PathVariable Long taskId, @RequestBody Task task) {
    return taskService.updateTask(ticketId, taskId, task);
  }

  @Operation(summary = "Deletes a task", description = "")
  @DeleteMapping("/tickets/{ticketId}/tasks/{taskId}")
  public Task deleteTask(@PathVariable String ticketId, @PathVariable Long taskId, @RequestBody Task task) {
//    return taskService.deleteTask(taskId);
    return taskService.deleteTask(ticketId, taskId);
  }
}
