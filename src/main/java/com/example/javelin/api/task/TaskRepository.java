package com.example.javelin.api.task;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
  Page<Task> findByTicketId(String ticketId, Pageable pageable);

  Optional<Task> findByIdAndTicketId(Long id, String postId);
}
