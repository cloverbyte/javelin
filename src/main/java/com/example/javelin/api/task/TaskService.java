package com.example.javelin.api.task;

import com.example.javelin.api.ticket.TicketRepository;
import com.example.javelin.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskService {
  private final TaskRepository taskRepository;
  private final TicketRepository ticketRepository;

  @Autowired
  public TaskService(TaskRepository taskRepository, TicketRepository ticketRepository) {
    this.taskRepository = taskRepository;
    this.ticketRepository = ticketRepository;
  }

  public Task createTask(String ticketId, Task task) {
    return ticketRepository.findById(ticketId).map(ticket -> {
      task.setTicket(ticket);
      return taskRepository.save(task);
    }).orElseThrow(() -> new ResourceNotFoundException("TicketId " + ticketId + " not found"));
  }

  public Page<Task> getAllTasksInTicket(String ticketId, Pageable pageable) {
    List<Task> tasks = new ArrayList<>();
    return taskRepository.findByTicketId(ticketId, pageable);
  }

//  public Task getTask(Long id) {
//    return taskRepository.findById(id).orElse(null);
//  }

  public Task updateTask(String ticketId, Long taskId, Task task) {
    return ticketRepository.findById(ticketId).map(ticket -> {
      if (!ticketRepository.existsById(ticketId)) {
        throw new ResourceNotFoundException("TicketId " + ticketId + " not found");
      }

      task.setTicket(ticket);
      return taskRepository.save(task);
    }).orElseThrow(() -> new ResourceNotFoundException("TicketId " + ticketId + " not found"));
  }

  public Task deleteTask(String ticketId, Long taskId) {
    return taskRepository.findByIdAndTicketId(taskId, ticketId).map(task -> {
      taskRepository.delete(task);
      return task;
    }).orElseThrow(() -> new ResourceNotFoundException("Task not found with id " + taskId + " and ticketId " + ticketId));
  }
}
