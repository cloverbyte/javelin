package com.example.javelin.api.ticket;

import com.example.javelin.api.note.Note;
import com.example.javelin.api.task.Task;
import com.example.javelin.common.Auditable;
import com.example.javelin.generator.TicketCodePrefixedSequenceIdGenerator;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Set;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Ticket extends Auditable<String> {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ticket_seq")
  @GenericGenerator(
      name = "ticket_seq",
      strategy = "com.example.javelin.generator.TicketCodePrefixedSequenceIdGenerator",
      parameters = {
          @Parameter(name = TicketCodePrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "50"),
          @Parameter(name = TicketCodePrefixedSequenceIdGenerator.SEPARATOR_PARAMETER, value = "-"),
          @Parameter(name = TicketCodePrefixedSequenceIdGenerator.DATE_FORMAT_PARAMETER, value = "%tY"),
          @Parameter(name = TicketCodePrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%05d")})
  private String id;

  private String type;
  private String summary;

//  private Author author;
//  private List<Article> articles;

//  @OneToMany(mappedBy = "ticket")
//  private Set<Note> notes;
//
//  @OneToMany(mappedBy = "ticket")
//  private Set<Task> tasks;

}
