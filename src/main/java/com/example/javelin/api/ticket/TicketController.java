package com.example.javelin.api.ticket;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tickets")
@Tag(name = "Ticket", description = "The Ticket API")
public class TicketController {
  private final TicketService ticketService;

  @Autowired
  public TicketController(TicketService ticketService) {
    this.ticketService = ticketService;
  }

  @Operation(summary = "Create a ticket", description = "Returns the created ticket")
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public Ticket createTicket(@RequestBody Ticket ticket) {
    return ticketService.createTicket(ticket);
  }

  @Operation(summary = "Find all tickets", description = "Returns all tickets")
  @GetMapping
  public List<Ticket> getAllTickets() {
    return ticketService.getAllTickets();
  }

  @Operation(summary = "Find ticket by ID", description = "Returns a single ticket.")
  @GetMapping("/{id}")
  public Ticket getTicket(@PathVariable String id) {
    return ticketService.getTicket(id);
  }

  @Operation(summary = "Update an existing ticket", description = "")
  @PutMapping("/{id}")
  public Ticket getTicket(@PathVariable String id, @RequestBody Ticket ticket) {
    return ticketService.updateTicket(id, ticket);
  }

  @Operation(summary = "Deletes a ticket", description = "")
  @DeleteMapping("/{id}")
  public Ticket deleteTicket(@PathVariable String id) {
    return ticketService.deleteTicket(id);
  }
}
