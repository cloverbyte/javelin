package com.example.javelin.api.ticket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TicketService {
  private final TicketRepository ticketRepository;

  @Autowired
  public TicketService(TicketRepository ticketRepository) {
    this.ticketRepository = ticketRepository;
  }

  public Ticket createTicket(Ticket ticket) {
    return ticketRepository.save(ticket);
  }

  public List<Ticket> getAllTickets() {
    List<Ticket> tickets = new ArrayList<>();
    ticketRepository.findAll().forEach(tickets::add);
    return tickets;
  }

  public Ticket getTicket(String id) {
    return ticketRepository.findById(id).orElse(null);
  }

  public Ticket updateTicket(String id, Ticket ticket) {
    Ticket ticketFromDb = ticketRepository.findById(id).orElse(null);
    ticket.setId(id);
    return ticketRepository.save(ticket);
  }

  public Ticket deleteTicket(String id) {
    Ticket ticketFromDb = ticketRepository.findById(id).orElse(null);
    ticketRepository.deleteById(id);
    return ticketFromDb;
  }

}
