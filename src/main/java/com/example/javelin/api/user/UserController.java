package com.example.javelin.api.user;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@Tag(name = "User", description = "The User API")
public class UserController {
  private final UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @Operation(summary = "Create a user", description = "Returns the created user")
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public User createUser(@RequestBody User user) {
    return userService.createUser(user);
  }

  @Operation(summary = "Find all users", description = "Returns all users")
  @GetMapping
  public List<User> getAllUsers() {
    return userService.getAllUsers();
  }

  @Operation(summary = "Find user by ID", description = "Returns a single user.")
  @GetMapping("/{id}")
  public User getUser(@PathVariable Long id) {
    return userService.getUser(id);
  }

  @Operation(summary = "Update an existing user", description = "")
  @PutMapping("/{id}")
  public User getUser(@PathVariable Long id, @RequestBody User user) {
    return userService.updateUser(id, user);
  }

  @Operation(summary = "Deletes a user", description = "")
  @DeleteMapping("/{id}")
  public User deleteUser(@PathVariable Long id) {
    return userService.deleteUser(id);
  }
}
