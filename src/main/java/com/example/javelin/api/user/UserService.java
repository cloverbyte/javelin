package com.example.javelin.api.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
  private final UserRepository userRepository;

  @Autowired
  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public User createUser(User user) {
    return userRepository.save(user);
  }

  public List<User> getAllUsers() {
    List<User> users = new ArrayList<>();
    userRepository.findAll().forEach(users::add);
    return users;
  }

  public User getUser(Long id) {
    return userRepository.findById(id).orElse(null);
  }

  public User updateUser(Long id, User user) {
    User userFromDb = userRepository.findById(id).orElse(null);
    user.setId(id);
    return userRepository.save(user);
  }

  public User deleteUser(Long id) {
    User userFromDb = userRepository.findById(id).orElse(null);
    userRepository.deleteById(id);
    return userFromDb;
  }
}
