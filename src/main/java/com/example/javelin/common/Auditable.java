package com.example.javelin.common;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable<U> implements Serializable {
  @CreatedBy
  @Column(name = "created_by", nullable = false, updatable = false)
  private U createdBy;

  @CreatedDate
  @Temporal(TIMESTAMP)
  @Column(name = "date_created", nullable = false, updatable = false)
  private Date dateCreated;

  @LastModifiedBy
  @Column(name = "last_modified_by", nullable = false)
  private U lastModifiedBy;

  @LastModifiedDate
  @Temporal(TIMESTAMP)
  @Column(name = "date_modified", nullable = false)
  private Date dateModified;

  @Column(name = "date_deleted")
  private Date dateDeleted;
}